import React from 'react';
import axios from 'axios';
import {
  Typography, TextField,
  InputAdornment, Paper, IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import ArrowForward from '@material-ui/icons/ArrowForward';

// COMPONENT IMPORTS
import UploadImageBuffer, { FileInfo } from './UploadButton';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: 25,
  },
});

/**
 * Helper function for converting an ArrayBuffer to base64
 * @param buffer ArrayBuffer to be converted
 * @returns base64 encoded data
 */
function arrayBufferToBase64( buffer: ArrayBuffer ) {
	let binary = '';
	const bytes = new Uint8Array( buffer );
	const len = bytes.byteLength;
	for (let i = 0; i < len; i++) {
		binary += String.fromCharCode( bytes[ i ] );
	}
	return window.btoa( binary );
}

interface InputMetadata {
  message:  string,
  error:    boolean,
}

export default function ShortLink(): JSX.Element {
  const styles = useStyles();
  const [ selectedImage, setSelectedImage ] = React.useState<FileInfo | null>(null);
  const [ redirectUrl, setRedirectUrl ] = React.useState<string>('');
  const [ shortUrlLink, setShortUrlLink ] = React.useState<string>('');
  const [ shortUrlInputMeta, setShortUrlInputMeta ] = React.useState<InputMetadata>({
    error: false,
    message: '',
  });
  const [ checkUrlAvailTimeout, setCheckUrlAvailTimeout ] = React.useState<NodeJS.Timeout | null>(null);
  const [ serverResponseMessage, setServerResponseMessage ] = React.useState<string>('');
  const server_url = process.env.REACT_APP_SERVER_ENDPOINT;
  const shorturl_endpoint = 'api/v1/short';

  // Handles submitting input
  const handleSubmission = () => {
    // Verify input required is present
    if (shortUrlInputMeta.error) {
      return;
    } else if (shortUrlLink.length === 0) {
      return setShortUrlInputMeta({
        error: true,
        message: 'Short-url is required',
      });
    } else if (redirectUrl.length === 0 && selectedImage === null) {
      return setServerResponseMessage('Redirect URL or Image required');
    }

    // Reset Server response
    setServerResponseMessage('');
    
    axios.post(`${server_url}/${shorturl_endpoint}`, {
      urlId: shortUrlLink,
      redirect: redirectUrl,
      imageBuffer: selectedImage?.buffer,
    })
      .then(() => setServerResponseMessage('Successfully created short-link!'))
      .catch(err => setServerResponseMessage( err.response.error || err.response.message) );
  };

  // Handles short url changes and verifies availability of the url
  const handleShortUrlChange = (url: string) => {
    setShortUrlLink(url);

    // Reset timeout
    if (checkUrlAvailTimeout) {
      clearTimeout(checkUrlAvailTimeout);
    }

    // Set timeout to check availability of url
    if (url.length === 0) return;
    setCheckUrlAvailTimeout(setTimeout(() => {
      axios.get(`${server_url}/${shorturl_endpoint}/${url}`)
        .then(res => {
          // Url is available
          if (res.data && res.data.message) {
            setShortUrlInputMeta({
              error: false,
              message: res.data.message,
            });
          } else {
            setShortUrlInputMeta({
              error: true,
              message: 'Short url not available',
            });
          }
        })
        .catch(err => {
          setShortUrlInputMeta({
            message: 'Internal error',
            error: true,
          });
          console.log('API Call Error:', err);
        });
    }, 1000));
  };
  
  return (<div className={styles.root}>
    <Typography variant='body1'>
      Generate a new short-url tied to an image or redirect to a url
    </Typography>
    <TextField
      label="New Short-URL"
      id="generate-short-url"
      onChange={e => handleShortUrlChange(e.target.value)}
      onSubmit={handleSubmission}
      InputProps={{
        startAdornment: <InputAdornment position="start">/api/v1/short/</InputAdornment>,
        endAdornment: (
          <IconButton onClick={handleSubmission}>
            <ArrowForward fontSize='small' />
          </IconButton>
        ),
      }}
      helperText={shortUrlInputMeta.message}
      error={shortUrlInputMeta.error}
    />

    {/* Set the short-url to redirect to this url */}
    <TextField
      label="Redirect to"
      id="redirect-url"
      onChange={e => setRedirectUrl(e.target.value)}
    />

    {/* Uploading an image will be voided by redirected url */}
    <UploadImageBuffer
      accept='image/*'
      onFileSelected={setSelectedImage}
      buttonText='Upload'
      disabled={redirectUrl.length !== 0}
      startIcon={<PhotoCamera />}
    />

    {/* NOTE: Server response here */}
    <Typography 
      variant='caption'
    >
      {serverResponseMessage}
    </Typography>

    {selectedImage && (
      <Paper elevation={2}>
        <img
          width={256}
          style={{
            opacity: redirectUrl.length > 0 ? 0.5 : 1.0,
          }}
          src={`data:${selectedImage.file.type};base64,` + arrayBufferToBase64(selectedImage.buffer)}
        />
      </Paper>
    )}
  </div>);
}