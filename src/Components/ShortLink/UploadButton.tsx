import React from 'react';
import { 
  makeStyles,
} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));


export interface FileInfo {
  name: string,
  buffer: ArrayBuffer,
  file: File,
}

interface IProps {
  accept?: string,
  onFileSelected: (file: FileInfo) => void,
  startIcon?: React.ReactNode,
  buttonText: string,
  disabled?: boolean,
}

export default function UploadButtons({ accept, onFileSelected, startIcon, buttonText, disabled }: IProps): JSX.Element {
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      <input
        accept={accept}
        className={classes.input}
        id="outlined-image-upload-button"
        type='file'
        disabled={disabled}
        onChange={e => {
          if (!e.target.files) return;
          
          const image = e.target.files[0];
          image.arrayBuffer()
            .then(buff => (onFileSelected({
              name: image.name,
              buffer: buff,
              file: image,
            })))
            .catch(err => console.log('Error converting image to buffer', err));
        }}
      />
      <label htmlFor="outlined-image-upload-button">
        <Button disabled={disabled} variant="outlined" color="primary" component="span" startIcon={startIcon} >
          {buttonText}
        </Button>
      </label>
    </div>
  );
}