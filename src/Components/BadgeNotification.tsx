import React from 'react';

// UI LIBRARIES
import { IconButton, Badge } from '@material-ui/core';

interface IProps {
  imageURL:         string,
  imageWidth:       number,
  badgeContent?:    string | number,
  maxContentValue?: number,   // Max number that content can go to before adding +, ie. '99+' | default = 100
  color?:           'primary' | 'secondary' | 'default' | 'error',
  onClick?:         () => void,
}


export default function BadgeNotification({ imageURL, imageWidth, color, badgeContent, maxContentValue, onClick } : IProps): JSX.Element {

  return (
    <IconButton style={{ padding: 4, borderRadius: 10 }} onClick={onClick} >
      <Badge style={{ padding: 0 }} max={maxContentValue} badgeContent={badgeContent} color={color} >
        <img src={imageURL} width={imageWidth} />
      </Badge>
    </IconButton>
  );
}