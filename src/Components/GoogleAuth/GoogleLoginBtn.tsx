import React from 'react';
import axios from 'axios';

// UI LIBRARIES
import { Button, makeStyles } from '@material-ui/core';

// ASSETS
import GoogleLogo from '../../Assets/google-200.png';

// STYLES
const useStyle = makeStyles({
  googleBtn: {
    backgroundColor: '#FF432D',
    color: '#EEE',
    fontFamily: 'Noto Sans, sans-serif',
    fontWeight: 800,
    borderRadius: 15,
    fontSize: 16,
    width: 125,
    textTransform: 'none',

    '&:hover': {
      backgroundColor: '#FA220A',
      borderColor: '#0062cc',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#AD2718',
      borderColor: '#1758AD',
    },

  },
});

export default function GoogleLoginButton(): JSX.Element {
  // Hooks
  const styles = useStyle();

  // Callbacks
  const onClick = () => {
    const SERVER_URL = process.env.REACT_APP_SERVER_ENDPOINT as string;

    // Login with OAuth2.0
    axios.get(`${SERVER_URL}/api/v1/auth`)
      .then(res => res.data)
      .then(data => {
        window.location.href = data.url;
      })
      .catch(err => console.log('Server Error:', err));
  };

  return (<>
    <Button
      className={styles.googleBtn}
      startIcon={<img src={GoogleLogo} width={30} />}
      onClick={onClick}
    > Google
    </Button>
  </>);
}