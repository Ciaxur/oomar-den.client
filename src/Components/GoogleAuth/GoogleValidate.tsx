import React from 'react';
import qs from 'qs';
import axios from 'axios';

// React-Router-Dom TYPES
import { Location, History } from 'history';

interface IProps {
  location: Location,
  history:  History,

  // Callback passing in HTTP Status Code
  onError?: (status: number) => void,
  onDone?:  (message: string) => void,
}

interface OAuthResponse {
  code:     string,
  prompt:   string,
  scope:   string,
}

export default function GoogleValidate(props: IProps): JSX.Element {
  // States
  const [ statusStr, setStatusStr ] = React.useState('Loading...');
  
  // Computed Data
  const response: OAuthResponse = React.useMemo(() => (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    qs.parse(location.search, { ignoreQueryPrefix: true }) as any
  ), [ props.location.search ]);

  React.useEffect(() => {
    const server_url = process.env.REACT_APP_SERVER_ENDPOINT;

    axios.post(`${server_url}/api/v1/auth/`, null, { params: { code: response.code }})
      .then(res => res.data)
      .then(data => {
        setStatusStr(data.message);
        
        if (props.onDone)
          props.onDone(data.message);
      })
      .catch(err => {
        console.log('Error: ', err.response);
        
        setStatusStr(err.response.data.message);
        if (props.onError) props.onError(err.response.data.status as number);
      });

    setStatusStr('Checking Google Auth Code Credentials. ✈️');
    
  }, [ response ]);
  

  return (<p>{statusStr}</p>);
}