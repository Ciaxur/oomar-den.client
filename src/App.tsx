import React from 'react';

// UI LIBRARIES
import {
  BrowserRouter as Router,
  Switch, Route, Redirect,
} from 'react-router-dom';

import {
  Container, Button,
  makeStyles, IconButton,
  Typography, Collapse,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import Alert, { Color } from '@material-ui/lab/Alert';

// COMPONENTS
import GoogleLoginButton from './Components/GoogleAuth/GoogleLoginBtn';
import GoogleAuthValidate from './Components/GoogleAuth/GoogleValidate';
import BadgeNotification from './Components/BadgeNotification';
import ShortLink from './Components/ShortLink';

// ASSETS
import PlanetLogo from './Assets/Planet-Logo64.png';
import BotLogo from './Assets/bot-100.png';
import TodoLogo from './Assets/todo-96.png';
import BlogMapLogo from './Assets/blogmap-icon.png';
import GithubLogo from './Assets/github-480.png';
import HomePageMeme from './Assets/ofcourse-mlady.jpg';
import axios from 'axios';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
  },
  alertContainer: {
    width: '100%',
    '& > * + *': {
      marginTop: 2,
    },
  },
  content: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: '25px',
  },
  logoImage: {
    marginBottom: 25,
    cursor: 'pointer',
  },

  badgeContainer: {
    display: 'flex',
    flexDirection: 'row',
    padding: '0px 10px',
    
    '& > *': {
      margin: '15px 5px',
    },
  },
  profileCnt: {
    padding: '10px 0px',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

// ENV Varialbes
const { 
  REACT_APP_SERVER_ENDPOINT: SERVER_URL,
} = process.env;

// Data Interface Obtained from Server
interface IData {
  user: {
    displayName:    string,
  },
  urls: {
    rt_todo:        string,
    blogmap: {
      value:        string,     // Link Value
      totalPapers:  number,     // Total Number of Paper Entries
    },
    oracle: {
      value:        string,     // Link Value
      totalGuilds:  number,     // Total Number of connected Servers
    },
    topgg: {
      value:        string,     // Link Value of Bot
      status:       number,     // Status of Bot (HTTP Status Code)
    }
  },
}

interface IAlertContainer {
  open:     boolean,
  severity: Color | undefined,
  message:  string,
}

function App(): JSX.Element {
  // States
  const [ authState, setAuthState ] = React.useState<number>(200);
  const [ data, setData ] = React.useState<IData | null>(null);
  const [ isLoadingData, setLoadingData ] = React.useState<boolean>(true);
  const [ alertContainer, setAlertContainer ] = React.useState<IAlertContainer>({
    message: '',
    severity: 'info',
    open: false,
  });
  
  // Hooks
  const styles = useStyles();
  
  // Component Events
  React.useEffect(() => {   // On Mount
    // Try and Get Data
    getData();
  }, []);
  
  // Methods
  const getData = async () => {
    return axios.get(`${SERVER_URL}/api/v1/data`)
      .then(res => res.data)
      .then(data => setData(data))
      .catch(err => console.log('getData Error:', err))
      .finally(() => setLoadingData(false));
  };

  const logoff = () => {
    axios.post(`${SERVER_URL}/api/v1/auth/logoff`)
      .then(_ => setData(null))
      .catch(err => console.log('Logoff Error:', err));
  };
  
  // Render
  return (
    <Router>
      <Container maxWidth='sm' className={styles.root} >

        {/* HEADER */}
        <Container className={styles.profileCnt}>
          {
            data !== null && <>
              <Typography variant='subtitle1'>
                {data.user.displayName}
              </Typography>
              <Button onClick={logoff}>
                Sign out
              </Button>
            </>
          }
        </Container>
        
        {/* MAIN CONTENT */}
        <Container className={styles.content} >
          <img className={styles.logoImage} src={PlanetLogo} onClick={() => (
            setData(null)
          )} />

          <Switch>

            <Route exact path='/' render={() => (
              isLoadingData
                ? <Typography variant='caption'>Trying to connect...</Typography>
                : (
                  data
                    ? <Redirect to='/home' />
                    : <>
                      <Typography variant='caption'>Connect using...</Typography>
                      <img src={HomePageMeme} width={128} style={{ marginBottom: '5px' }} />
                      <GoogleLoginButton />
                    </>)
            )
            } />

            <Route path='/auth' render={props => (<>
              <GoogleAuthValidate 
                {...props} 
                onError={status => setAuthState(status)} 
                onDone={() => {
                  getData()
                    .then(() => props.history.push('/home'));
                }}
              />
              
              { authState !== 200 && <Button onClick={() => props.history.push('/')}>Go Back</Button> }
            </>)} />

            <Route path='/home' render={() => (
              data
                ? <>
                  <div className={styles.badgeContainer} >
                    <BadgeNotification
                      imageURL={TodoLogo}
                      imageWidth={24}
                      color='secondary'
                      onClick={data
                        ? () => window.open(data.urls.rt_todo, '_blank')
                        : undefined
                      }
                    />

                    <BadgeNotification
                      badgeContent={data.urls.oracle.totalGuilds}
                      imageURL={GithubLogo}
                      imageWidth={24}
                      color='secondary'
                      onClick={data
                        ? () => window.open(data.urls.oracle.value, '_blank')
                        : undefined
                      }
                    />

                    <BadgeNotification
                      badgeContent={data.urls.topgg.status || '?'}
                      maxContentValue={1000}
                      imageURL={BotLogo}
                      imageWidth={24}
                      color='secondary'
                      onClick={data
                        ? () => window.open(data.urls.topgg.value, '_blank')
                        : undefined
                      }
                    />

                    <BadgeNotification
                      badgeContent={data.urls.blogmap.totalPapers !== -1
                        ? data.urls.blogmap.totalPapers
                        : undefined
                      }
                      imageURL={BlogMapLogo}
                      imageWidth={24}
                      color='secondary'
                      onClick={data
                        ? () => window.open(data.urls.blogmap.value, '_blank')
                        : undefined
                      }
                    />
                  </div>
                </>
                : <Redirect to='/' />
            )} />

            <Route path='/short' render={() => (<>
              <ShortLink />
            </>)} />

            <Route path='*'>
              <div className={styles.content} style={{ gap: 5 }}>
                <img src='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.tenor.com%2Fimages%2F40ba58ee57a145220c2926587770a87d%2Ftenor.gif&f=1&nofb=1' />
                <Typography variant='body1'>
                  Route not found
                </Typography>
              </div>
            </Route>
          </Switch>
        </Container>

        {/* FOOTER */}
        <Container className={styles.content} >
          <div className={styles.alertContainer}>
            <Collapse in={alertContainer.open}>
              <Alert
                severity={alertContainer.severity}
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setAlertContainer({
                        ...alertContainer,
                        open: false,
                      });
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
              >
                {alertContainer.message}
              </Alert>
            </Collapse>
          </div>
        </Container>
        
      </Container>

    </Router>
  );
}

export default App;
